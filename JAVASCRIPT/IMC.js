// Obtener el objeto button de calcular
const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click', function () {
    let peso = parseFloat(document.getElementById('peso').value);
    let altura = parseFloat(document.getElementById('altura').value);

    // Hacer los cálculos
    let imc = peso / (altura * altura);

    // Mostrar el resultado en el campo "imcResultado"
    document.getElementById('imcResultado').value = imc.toFixed(2);
});

// Mostrar imagenes

    const imcResultado = document.getElementById("imcResultado");
    const sexoMasculino = document.getElementById("sexoMasculino");

    const images = {
        masculino: {
            bajo: "/img/01H.png",
            normal: "/img/02H.png",
            sobrepeso: "/img/03H.png",
            obesidad: "/img/04H.png",
            severa:"/img/05H.png",
            morbida: "/img/06H.png",
        },
        femenino: {
            bajo: "/img/01M.png",
            normal: "/img/02M.png",
            sobrepeso: "/img/03M.png",
            obesidad: "/img/04M.png",
            severa:"/img/05M.png",
            morbida: "/img/06M.png",
        },
    };

    btnCalcular.addEventListener("click", function () {
        let peso = parseFloat(document.getElementById("peso").value);
        let altura = parseFloat(document.getElementById("altura").value);
    
        // Hacer los cálculos
        let imc = peso / (altura * altura);
    
        // Mostrar el resultado en the campo "imcResultado"
        document.getElementById("imcResultado").value = imc.toFixed(2);
    
        // Mostrar la imagen
        const imagen = document.getElementById("img");
    
        let selectedImage = "";
    
        if (imc >= 40) {
            if (sexoMasculino.checked) {
                selectedImage = images.masculino.morbida;
            } else {
                selectedImage = images.femenino.morbida;
            }
        } else if (imc >= 35 && imc < 40) {
            if (sexoMasculino.checked) {
                selectedImage = images.masculino.severa;
            } else {
                selectedImage = images.femenino.severa;
            }
        } else if (imc >= 30 && imc < 35) {
            if (sexoMasculino.checked) {
                selectedImage = images.masculino.obesidad;
            } else {
                selectedImage = images.femenino.obesidad;
            }
        } else if (imc >= 25 && imc < 30) {
            if (sexoMasculino.checked) {
                selectedImage = images.masculino.sobrepeso;
            } else {
                selectedImage = images.femenino.sobrepeso;
            }
        } else if (imc >= 18.5 && imc < 25) {
            if (sexoMasculino.checked) {
                selectedImage = images.masculino.normal;
            } else {
                selectedImage = images.femenino.normal;
            }
        } else if (imc < 18.5) {
            if (sexoMasculino.checked) {
                selectedImage = images.masculino.bajo;
            } else {
                selectedImage = images.femenino.bajo;
            }
        }
    
        // Display the selected image
        imagen.src = selectedImage;
    });
    
    btnCalcular.addEventListener("click", function () {
        let peso = parseFloat(document.getElementById("peso").value);
        let edad = parseFloat(document.getElementById("edad").value);
        let calorias = 0;
        
        if (edad>=10 && edad<18){
            if (sexoMasculino.checked) {
                calorias = (17.686 * peso) + 658.2;
            } else {
                calorias = (13.384 * peso) + 692.6;
            }
        } else if(edad>=18 && edad<30) {
            if (sexoMasculino.checked) {
                calorias = (15.057 * peso) + 692.2;
            } else {
                calorias = (14.818 * peso) + 486.6;
            }
        }else if(edad>=30 && edad<60) {
            if (sexoMasculino.checked) {
                calorias = (11.472 * peso) + 873.1;
            } else {
                calorias = (8.126 * peso) + 845.6;
            }
        }else if(edad>=60){
            if (sexoMasculino.checked) {
                calorias = (11.711 * peso) + 587.7;
            } else {
                calorias = (9.082 * peso) + 658.5;
            }
        }
        
        document.getElementById('calorias').value = calorias.toFixed(2);

    });
