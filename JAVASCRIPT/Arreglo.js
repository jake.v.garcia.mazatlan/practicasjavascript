const arreglo = [];

function generarArreglo() {
    const tamano = parseInt(document.getElementById("tamaño").value, 10);
    arreglo.length = 0;

    for (let i = 0; i < tamano; ++i) {
        const numeroAleatorio = Math.floor(Math.random() * 1000);
        arreglo.push(numeroAleatorio);
    }

 //¿ARREGLO GENERADO
 const resultado = document.getElementById("resultado");
 //resultado.innerHTML = "Arreglo generado: " + "[" + arreglo.join(", ") + "]";
 
 
 // MOSTRAR ARREGLO
 const select = document.getElementById("array");
 select.innerHTML = '';
 arreglo.forEach((numero, index) => {
     const option = document.createElement("option");
     option.value = index;
     option.text = numero;
     select.appendChild(option);
 });
mostrarProm();
mostrarMay();
mostrarMen();
mostrarSimetria();
mostrarPar();
mostrarImpar();
}
//PROMEDIO
function promedio(){
    let suma = 0;
    for(let i=0 ; i < arreglo.length ; ++i ){
        suma += arreglo[i];
    }
    const pro = suma/arreglo.length;
    return pro;
}
//MOSTRAR EL PROMEDIO
function mostrarProm(){
    if(arreglo.length>0){
        const Prom = promedio();
        const Pro = document.getElementById("pro");
        Pro.value = Prom.toFixed(2);
    }
}

mostrarProm();


// MAYOR Y POSICION
function mayor() {
    let mayor = 0;
    let posicion = 0;
    for (let i = 0; i < arreglo.length; ++i) {
        if (arreglo[i] >= mayor) {
            mayor = arreglo[i];
            posicion = i;
        }
    }
    return { valor: mayor, posicion: posicion };
}
//MOSTRAR LA MOY POSICION
function mostrarMay(){
    if(arreglo.length>0){
        const Mayor = mayor();
        const may = document.getElementById("may");
        const pos = document.getElementById("pos")
        may.value = Mayor.valor.toFixed(2);
        pos.value = Mayor.posicion;
    }
}

// MENOR Y POSICION
function menor() {
    let menor = arreglo[0];
    let posicion = 0;
    for (let i = 0; i < arreglo.length; ++i) {
        if (arreglo[i] < menor) {
            menor = arreglo[i];
            posicion = i;
        }
    }
    return { valor: menor, posicion: posicion };
}
//MOSTRAR LA POSICION MENOR
function mostrarMen(){
    if(arreglo.length>0){
        const Menor = menor();
        const men = document.getElementById("men");
        const Posi = document.getElementById("Posi")
        men.value = Menor.valor.toFixed(2);
        Posi.value = Menor.posicion;
    }
}


generarArreglo();

//PORCENTAJE DE SIMETRIA

function simetria(){
    if(arreglo.length === 0){
        return 0; //SI EL ARREGLO ES VACIO NO HAY UNA SIMETRIA
    }
    //VALOR DEL ARREGLO
    const suma = arreglo.reduce((acumulado, numero) => acumulado + numero , 0);
    const valorMedio = suma / arreglo.length;
    const rango = 10;
    const numRango = arreglo.filter(numero => Math.abs(numero - valorMedio) <= rango);
    const Simetria = (numRango.length / arreglo.length)*100;
    return Simetria;

}
//MOSTRAR LA SIMETRIA
function mostrarSimetria() {
    const Simetrico = simetria();
    const simetrico = document.getElementById("simetrico");
    
    if (Simetrico === 100) {
        simetrico.value = "Sí";
    } else {
        simetrico.value = "No";
    }
}
//PARES
function par() {
    if (arreglo.length > 0) {
        let pares = 0;
        for (let i = 0; i < arreglo.length; ++i) {
            if (arreglo[i] % 2 === 0) {
                pares++;
            }
        }
        return pares;
    }
    return 0;
}
//MOSTRAR PARES
function mostrarPar() {
    if (arreglo.length > 0) {
        const total = arreglo.length;
        const cantPares = par();
        const porcentaje = (cantPares / total) * 100;
        const pares = document.getElementById("pares");
        pares.value = porcentaje.toFixed(2) + "%";
    }
}
//PORCENTAJE DE IMPARES
function impar() {
    if (arreglo.length > 0) {
        let impares = 0;
        for (let i = 0; i < arreglo.length; ++i) {
            if (arreglo[i] % 2 !== 0) {
                impares++;
            }
        }
        return impares;
    }
    return 0;
}
//MOSTRAR IMPARES
function mostrarImpar() {
    if (arreglo.length > 0) {
        const total = arreglo.length;
        const cantImpares = impar();
        const porcentaje = (cantImpares / total) * 100;
        const impares = document.getElementById("impares");
        impares.value = porcentaje.toFixed(2) + "%";
    }
}